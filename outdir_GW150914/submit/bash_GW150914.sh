#!/usr/bin/env bash

# GW150914_data0_1126259462-4_generation
# PARENTS 
# CHILDREN GW150914_data0_1126259462-4_sampling GW150914_data0_1126259462-4_importance_sampling_part0 GW150914_data0_1126259462-4_importance_sampling_part1 GW150914_data0_1126259462-4_importance_sampling_part2 GW150914_data0_1126259462-4_importance_sampling_part3 GW150914_data0_1126259462-4_importance_sampling_part4 GW150914_data0_1126259462-4_importance_sampling_part5 GW150914_data0_1126259462-4_importance_sampling_part6 GW150914_data0_1126259462-4_importance_sampling_part7 GW150914_data0_1126259462-4_importance_sampling_part8 GW150914_data0_1126259462-4_importance_sampling_part9
if [[ "GW150914_data0_1126259462-4_generation" == *"$1"* ]]; then
    echo "Running: /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_generation outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_generation --idx 0 --trigger-time 1126259462.4 --outdir outdir_GW150914"
    /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_generation outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_generation --idx 0 --trigger-time 1126259462.4 --outdir outdir_GW150914
fi

# GW150914_data0_1126259462-4_sampling
# PARENTS GW150914_data0_1126259462-4_generation
# CHILDREN GW150914_data0_1126259462-4_importance_sampling_part0 GW150914_data0_1126259462-4_importance_sampling_part1 GW150914_data0_1126259462-4_importance_sampling_part2 GW150914_data0_1126259462-4_importance_sampling_part3 GW150914_data0_1126259462-4_importance_sampling_part4 GW150914_data0_1126259462-4_importance_sampling_part5 GW150914_data0_1126259462-4_importance_sampling_part6 GW150914_data0_1126259462-4_importance_sampling_part7 GW150914_data0_1126259462-4_importance_sampling_part8 GW150914_data0_1126259462-4_importance_sampling_part9
if [[ "GW150914_data0_1126259462-4_sampling" == *"$1"* ]]; then
    echo "Running: /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_sampling --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5"
    /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_sampling --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5
fi

# GW150914_data0_1126259462-4_importance_sampling_part0
# PARENTS GW150914_data0_1126259462-4_sampling GW150914_data0_1126259462-4_generation
# CHILDREN GW150914_data0_1126259462-4_importance_sampling_merge
if [[ "GW150914_data0_1126259462-4_importance_sampling_part0" == *"$1"* ]]; then
    echo "Running: /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part0 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part0.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5"
    /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part0 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part0.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5
fi

# GW150914_data0_1126259462-4_importance_sampling_part1
# PARENTS GW150914_data0_1126259462-4_sampling GW150914_data0_1126259462-4_generation
# CHILDREN GW150914_data0_1126259462-4_importance_sampling_merge
if [[ "GW150914_data0_1126259462-4_importance_sampling_part1" == *"$1"* ]]; then
    echo "Running: /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part1 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part1.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5"
    /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part1 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part1.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5
fi

# GW150914_data0_1126259462-4_importance_sampling_part2
# PARENTS GW150914_data0_1126259462-4_sampling GW150914_data0_1126259462-4_generation
# CHILDREN GW150914_data0_1126259462-4_importance_sampling_merge
if [[ "GW150914_data0_1126259462-4_importance_sampling_part2" == *"$1"* ]]; then
    echo "Running: /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part2 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part2.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5"
    /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part2 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part2.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5
fi

# GW150914_data0_1126259462-4_importance_sampling_part3
# PARENTS GW150914_data0_1126259462-4_sampling GW150914_data0_1126259462-4_generation
# CHILDREN GW150914_data0_1126259462-4_importance_sampling_merge
if [[ "GW150914_data0_1126259462-4_importance_sampling_part3" == *"$1"* ]]; then
    echo "Running: /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part3 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part3.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5"
    /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part3 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part3.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5
fi

# GW150914_data0_1126259462-4_importance_sampling_part4
# PARENTS GW150914_data0_1126259462-4_sampling GW150914_data0_1126259462-4_generation
# CHILDREN GW150914_data0_1126259462-4_importance_sampling_merge
if [[ "GW150914_data0_1126259462-4_importance_sampling_part4" == *"$1"* ]]; then
    echo "Running: /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part4 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part4.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5"
    /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part4 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part4.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5
fi

# GW150914_data0_1126259462-4_importance_sampling_part5
# PARENTS GW150914_data0_1126259462-4_sampling GW150914_data0_1126259462-4_generation
# CHILDREN GW150914_data0_1126259462-4_importance_sampling_merge
if [[ "GW150914_data0_1126259462-4_importance_sampling_part5" == *"$1"* ]]; then
    echo "Running: /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part5 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part5.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5"
    /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part5 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part5.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5
fi

# GW150914_data0_1126259462-4_importance_sampling_part6
# PARENTS GW150914_data0_1126259462-4_sampling GW150914_data0_1126259462-4_generation
# CHILDREN GW150914_data0_1126259462-4_importance_sampling_merge
if [[ "GW150914_data0_1126259462-4_importance_sampling_part6" == *"$1"* ]]; then
    echo "Running: /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part6 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part6.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5"
    /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part6 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part6.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5
fi

# GW150914_data0_1126259462-4_importance_sampling_part7
# PARENTS GW150914_data0_1126259462-4_sampling GW150914_data0_1126259462-4_generation
# CHILDREN GW150914_data0_1126259462-4_importance_sampling_merge
if [[ "GW150914_data0_1126259462-4_importance_sampling_part7" == *"$1"* ]]; then
    echo "Running: /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part7 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part7.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5"
    /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part7 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part7.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5
fi

# GW150914_data0_1126259462-4_importance_sampling_part8
# PARENTS GW150914_data0_1126259462-4_sampling GW150914_data0_1126259462-4_generation
# CHILDREN GW150914_data0_1126259462-4_importance_sampling_merge
if [[ "GW150914_data0_1126259462-4_importance_sampling_part8" == *"$1"* ]]; then
    echo "Running: /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part8 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part8.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5"
    /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part8 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part8.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5
fi

# GW150914_data0_1126259462-4_importance_sampling_part9
# PARENTS GW150914_data0_1126259462-4_sampling GW150914_data0_1126259462-4_generation
# CHILDREN GW150914_data0_1126259462-4_importance_sampling_merge
if [[ "GW150914_data0_1126259462-4_importance_sampling_part9" == *"$1"* ]]; then
    echo "Running: /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part9 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part9.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5"
    /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_importance_sampling outdir_GW150914/GW150914_config_complete.ini --local-generation --label GW150914_data0_1126259462-4_importance_sampling_part9 --proposal-samples-file outdir_GW150914/result/GW150914_data0_1126259462-4_sampling_part9.hdf5 --event-data-file outdir_GW150914/data/GW150914_data0_1126259462-4_generation_event_data.hdf5
fi

# GW150914_data0_1126259462-4_importance_sampling_merge
# PARENTS GW150914_data0_1126259462-4_importance_sampling_part0 GW150914_data0_1126259462-4_importance_sampling_part1 GW150914_data0_1126259462-4_importance_sampling_part2 GW150914_data0_1126259462-4_importance_sampling_part3 GW150914_data0_1126259462-4_importance_sampling_part4 GW150914_data0_1126259462-4_importance_sampling_part5 GW150914_data0_1126259462-4_importance_sampling_part6 GW150914_data0_1126259462-4_importance_sampling_part7 GW150914_data0_1126259462-4_importance_sampling_part8 GW150914_data0_1126259462-4_importance_sampling_part9
# CHILDREN GW150914_data0_1126259462-4_importance_sampling_plot GW150914_pesummary
if [[ "GW150914_data0_1126259462-4_importance_sampling_merge" == *"$1"* ]]; then
    echo "Running: /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_result --result outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part0.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part1.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part2.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part3.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part4.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part5.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part6.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part7.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part8.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part9.hdf5 --outdir outdir_GW150914/result --label GW150914_data0_1126259462-4_importance_sampling_merge --extension hdf5 --merge"
    /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_result --result outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part0.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part1.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part2.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part3.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part4.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part5.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part6.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part7.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part8.hdf5 outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling_part9.hdf5 --outdir outdir_GW150914/result --label GW150914_data0_1126259462-4_importance_sampling_merge --extension hdf5 --merge
fi

# GW150914_data0_1126259462-4_importance_sampling_plot
# PARENTS GW150914_data0_1126259462-4_importance_sampling_merge
# CHILDREN 
if [[ "GW150914_data0_1126259462-4_importance_sampling_plot" == *"$1"* ]]; then
    echo "Running: /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_plot --label GW150914_data0_1126259462-4_importance_sampling_plot --result outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling.hdf5 --outdir outdir_GW150914/result --corner --weights --log_probs"
    /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/dingo_pipe_plot --label GW150914_data0_1126259462-4_importance_sampling_plot --result outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling.hdf5 --outdir outdir_GW150914/result --corner --weights --log_probs
fi

# GW150914_pesummary
# PARENTS GW150914_data0_1126259462-4_importance_sampling_merge
# CHILDREN 
if [[ "GW150914_pesummary" == *"$1"* ]]; then
    echo "Running: /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/summarypages --webdir outdir_GW150914/results_page --config outdir_GW150914/GW150914_config_complete.ini --samples outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling.hdf5 -a IMRPhenomXPHM"
    /work/nihargupte/environments/micromamba/envs/dingo-unstable/bin/summarypages --webdir outdir_GW150914/results_page --config outdir_GW150914/GW150914_config_complete.ini --samples outdir_GW150914/result/GW150914_data0_1126259462-4_importance_sampling.hdf5 -a IMRPhenomXPHM
fi

